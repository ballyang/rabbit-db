/**
 * @(#) MySQLPacket.java RabbitDB
 */
package cn.rabbitdb.protocol.mysql41;

import cn.rabbitdb.protocol.MySQLMessageReader;
import cn.rabbitdb.protocol.MySQLMessageWriter;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

/**
 * @author 智慧工厂
 *
 */
@Getter
public abstract class MySQLPacket {
	@Setter
	private int packetLength;
	private int packetId;
	private final MySQLMessageReader reader;

	public MySQLPacket(int packetLength, int packetId) {
		super();
		this.packetLength = packetLength;
		this.packetId = packetId;
		this.reader = null;
	}

	public MySQLPacket(ByteBuf buffer) {
		this.reader = new MySQLMessageReader(buffer);
		this.packetLength = reader.readUB3();
		this.packetId = reader.readUB();
	}

	public abstract MySQLMessageWriter getMySQLMessageWriter();

	public abstract int calucatePacketLength();

}
