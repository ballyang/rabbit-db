/**
 * @(#) SessionManager.java RabbitDB
 */
package cn.rabbitdb.faced.server;

import java.util.Hashtable;

import io.netty.channel.ChannelHandlerContext;
import lombok.Data;

/**
 * 客户端会话管理器
 * 
 * @author 智慧工厂@
 *
 */
@Data
class SessionManager {
	public static SessionManager instance = new SessionManager();
	private final Hashtable<ChannelHandlerContext, Session> sessions = new Hashtable<>();

	private SessionManager() {
	}
}
