package cn.rabbitdb.faced.server.configure;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * 服务配置项
 * @author jactly
 */
public class ServerConfig {
    @Getter
    @Setter
    private int port = 3308;
}
