/**
 * @(#) Lifecycle.java RabbitDB
 */
package cn.rabbitdb.faced.server;

/**
 * 生命周期控制
 * 
 * @author 智慧工厂@
 *
 */
public interface Lifecycle {
	/**
	 * 启动
	 */
	void startup();

	/**
	 * 关闭
	 */
	void shutdown();

	/**
	 * 重启
	 */
	void restart();
}
