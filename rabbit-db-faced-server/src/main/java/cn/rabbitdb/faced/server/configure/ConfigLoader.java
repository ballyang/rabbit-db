package cn.rabbitdb.faced.server.configure;

import com.beust.jcommander.JCommander;

/**
 * 配置加载器
 */
public class ConfigLoader {

    public static ServerConfig load(String[] args){
        ServerConfig config = new ServerConfig();
        readByCommand(config, args);
        return config;
    }

    private static void readByCommand(ServerConfig config, String[] args){
        CommandArgs commandArgs = new CommandArgs();
        JCommander.newBuilder().addObject(commandArgs).build().parse(args);
        if(commandArgs.getPort() != null && 0 <= commandArgs.getPort() && commandArgs.getPort() <= 65535){
            config.setPort(commandArgs.getPort());
        }
    }

}
