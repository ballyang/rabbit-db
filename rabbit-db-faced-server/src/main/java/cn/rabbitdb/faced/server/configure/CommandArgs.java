package cn.rabbitdb.faced.server.configure;

import com.beust.jcommander.Parameter;
import lombok.Getter;

public class CommandArgs {
    @Getter
    @Parameter(names = {"-P", "--port"}, description = "Port number to use for connection to, built-in default (3308).")
    private Integer port;
}
