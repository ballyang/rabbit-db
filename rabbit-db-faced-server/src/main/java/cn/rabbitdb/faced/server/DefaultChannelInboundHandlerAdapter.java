/**
 * @(#) DefaultChannelInboundHandlerAdapter.java RabbitDB
 */
package cn.rabbitdb.faced.server;

import cn.rabbitdb.faced.server.utils.Bytes;
import cn.rabbitdb.protocol.mysql41.MySQLHandshark41;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 智慧工厂@
 *
 */
@RequiredArgsConstructor
@Getter
class DefaultChannelInboundHandlerAdapter extends ChannelInboundHandlerAdapter {
	private final Lifecycle boundServer;

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		/**
		 * 根据MYSQL41协议: 随即生成密码加密种子
		 */
		byte[] authPluginDataPart1 = Bytes.random(8);
		byte[] authPluginDataPart2 = Bytes.random(12);

		/**
		 * 写入会话管理器
		 */
		SessionManager.instance.getSessions().put(ctx, new Session(ctx, authPluginDataPart1, authPluginDataPart2));
		/**
		 * 根据MYSQL41协议: 当客户端连接后,立即发送握手包
		 */
		MySQLHandshark41 handshark = new MySQLHandshark41(0, 10, "1.0.0", (int) Thread.currentThread().getId(),
				authPluginDataPart1, -15361, 33 /* UTF-8 */, 2, 21, authPluginDataPart2, "caching_sha2_password");
		ctx.writeAndFlush(handshark.getMySQLMessageWriter().getBuffer());
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		ByteBuf buf = (ByteBuf) msg;
	}
}
