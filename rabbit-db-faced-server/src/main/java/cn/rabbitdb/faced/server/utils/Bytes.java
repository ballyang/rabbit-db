/**
 * @(#) Bytes.java RabbitDB
 */
package cn.rabbitdb.faced.server.utils;

import java.util.Random;

/**
 * @author 智慧工厂@
 *
 */
public class Bytes {
	public static byte[] random(int length) {
		Random random = new Random(255);
		byte[] buffer = new byte[length];
		for (int i = 0; i < length; i++) {
			buffer[i] = (byte) random.nextInt();
		}
		return buffer;
	}
}
